<?php

namespace Drupal\extraconfigfield;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an extraconfigfield entity type.
 */
interface ExtraConfigFieldInterface extends ConfigEntityInterface {
}
